#include "hovering.h"
#include "manager_inc.h"


using namespace std;


Hovering::Hovering(DroneManager *manager_mach) : drone_manager(manager_mach)
{
    state_step = 0;
}

Hovering::~Hovering(void)
{

}

bool Hovering::land()
{
    cout<<"From HOVERING to LANDING"<<endl;
    drone_manager->setCurrentState(new Landing(drone_manager));
    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDING);

    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Hovering::loop()
{
    cout<<"From HOVERING to LOOPING"<<endl;

    drone_manager ->setCurrentState(new Looping(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_FLIP);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool Hovering::hover()
{
//    cout<<"Alredy in HOVERING mode"<<endl;
//    return false;
    cout<<"From HOVER to HOVER"<<endl;

    drone_manager->setCurrentState(new Hovering(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::HOVERING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool Hovering::move_manual_altitude()
{
    cout<<"From Hovering to Moving MANUAL IN ALTITUD"<<endl;

    Moving* moving_state = new MovingManualAltitud(drone_manager);
    drone_manager->setCurrentState(moving_state);


    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Hovering::move_position()
{
    cout<<"From Hovering to Moving in POSITION"<<endl;

    Moving* moving_state = new MovingPosition(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_POSITION);
    drone_manager->getCurrentState()->do_continue();


    delete this;
    return true;
}

bool Hovering::move_speed()
{
    cout<<"From Hovering to Moving in SPEED"<<endl;

    Moving* moving_state = new MovingSpeed(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_SPEED);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool Hovering::move_trajectory()
{
    cout<<"From Hovering to Moving in TRAJECTORY"<<endl;

    Moving* moving_state = new MovingTrajectory(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Hovering::move_visual_servoing()
{
    cout<<"From Hovering to Moving in VISUAL SERVOING"<<endl;

    Moving* moving_state = new MovingVisualServoing (drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Hovering::emergency()
{
    cout<<"From HOVERING to EMERGENCY"<<endl;

    drone_manager ->setCurrentState(new Emergency(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::EMERGENCY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Hovering::do_continue()
{
#ifdef DEBUG_MODE
    cout<<"HOVERING Sequence"<<endl;
#endif
//    droneMsgsROS::askForModule srv;
//    srv.request.module_name = MODULE_NAME_TRAJECTORY_CONTROLLER;
//    if(drone_manager->getModuleIsStartedClientSrv().call(srv))

    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_optional_modules_started = 0;


//    drone_manager->drone_hover();
//    drone_manager->clearCmd();

    switch(state_step)
    {
        case 0:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
#ifdef DEBUG_MODE
            cout<<"HOVER Sequence-Step 0 finished"<<endl;
#endif
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
#endif
                    counter_optional_modules_started++;
                }
                else
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" NOT STARTED"<<endl;
                    drone_manager->printModulesInList();
#endif
                    drone_manager->startModule(optional_modules_names[i]);


                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::HOVERING);
//                if(drone_manager->getFlagCommandReceived())
//                {
//                    drone_manager->publishDroneMPCommandAck(true);
//                    drone_manager->setFlagCommandReceived(false);
//                }
//                cout<<"Supervisor: You can proceed!"<<endl;
#ifdef DEBUG_MODE
                cout<<"HOVER Sequence-Step 1 finished"<<endl;
#endif
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
            drone_manager->clearCmd();
            drone_manager->drone_hover();
            if(drone_manager->getFlagCommandReceived())
            {
                drone_manager->publishDroneMPCommandAck(true);
                drone_manager->setFlagCommandReceived(false);
            }
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::HOVERING);
#ifdef DEBUG_MODE
            cout<<"Supervisor: You can proceed!"<<endl;
#endif
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;



    }


}







