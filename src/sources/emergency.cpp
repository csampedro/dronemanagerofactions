#include "emergency.h"
#include "manager_inc.h"


using namespace std;


Emergency::Emergency(DroneManager *manager_mach) : drone_manager(manager_mach)
{
}

Emergency::~Emergency(void)
{

}

bool Emergency::land()
{
    cout<<"From EMERGENCY to LANDING"<<endl;

    delete this;
    return true;
}

bool Emergency::hover()
{
    cout<<"From EMERGENCY to HOVER"<<endl;

    drone_manager ->setCurrentState(new Hovering(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::HOVERING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Emergency::do_continue()
{
    cout<<"EMERGENCY Sequence"<<endl;

    drone_manager->stopModule(MODULE_NAME_TRAJECTORY_CONTROLLER);
    //drone_manager->drone_hover();
    //drone_manager->drone_emergency();
    drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::EMERGENCY);
}









