#include "landing_autonomous.h"
#include "manager_inc.h"


using namespace std;


LandingAutonomous::LandingAutonomous(DroneManager *manager_mach) : drone_manager(manager_mach)
{
    state_step = 0;
}

LandingAutonomous::~LandingAutonomous(void)
{

}

bool LandingAutonomous::land()
{
    cout<<"From LANDING AUTONOMOUS to LANDING"<<endl;
    drone_manager->setCurrentState(new Landing(drone_manager));
    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDING);

    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool LandingAutonomous::land_autonomous()
{
//    cout<<"Alredy in LandingAutonomous mode"<<endl;
//    return false;
    cout<<"From LANDING AUTONOMOUS to LANDING AUTONOMOUS"<<endl;

    drone_manager->setCurrentState(new LandingAutonomous(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDING_AUTONOMOUS);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool LandingAutonomous::move_manual_altitude()
{
    cout<<"From LANDING AUTONOMOUS to Moving MANUAL IN ALTITUD"<<endl;

    Moving* moving_state = new MovingManualAltitud(drone_manager);
    drone_manager->setCurrentState(moving_state);


    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool LandingAutonomous::move_position()
{
    cout<<"From LANDING AUTONOMOUS to Moving in POSITION"<<endl;

    Moving* moving_state = new MovingPosition(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_POSITION);
    drone_manager->getCurrentState()->do_continue();


    delete this;
    return true;
}

bool LandingAutonomous::move_speed()
{
    cout<<"From LANDING AUTONOMOUS to Moving in SPEED"<<endl;

    Moving* moving_state = new MovingSpeed(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_SPEED);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool LandingAutonomous::move_trajectory()
{
    cout<<"From LANDING AUTONOMOUS to Moving in TRAJECTORY"<<endl;

    Moving* moving_state = new MovingTrajectory(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool LandingAutonomous::move_visual_servoing()
{
    cout<<"From LANDING AUTONOMOUS to Moving in VISUAL SERVOING"<<endl;

    Moving* moving_state = new MovingVisualServoing (drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool LandingAutonomous::emergency()
{
    cout<<"From LANDING AUTONOMOUS to EMERGENCY"<<endl;

    drone_manager ->setCurrentState(new Emergency(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::EMERGENCY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool LandingAutonomous::do_continue()
{
    cout<<"LANDING AUTONOMOUS Sequence"<<endl;
    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_optional_modules_started = 0;


    switch(state_step)
    {
        case 0:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
            cout<<"LANDING AUTONOMOUS Sequence-Step 0 finished"<<endl;
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
                    counter_optional_modules_started++;
                }
                else
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" NOT STARTED"<<endl;
                    drone_manager->printModulesInList();
                    drone_manager->startModule(optional_modules_names[i]);
                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
                cout<<"LANDING AUTONOMOUS Sequence-Step 1 finished"<<endl;
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
            if(drone_manager->getFlagCommandReceived())
            {
                drone_manager->publishDroneMPCommandAck(true);
                drone_manager->setFlagCommandReceived(false);
            }
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::LANDING_AUTONOMOUS);
            cout<<"Supervisor: You can proceed!"<<endl;
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;



    }


}







