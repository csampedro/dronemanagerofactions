//#include "droneManager.h"
#include "manager_inc.h"

using namespace std;
#define BATTERY_LEVEL_CHECK_THRESHOLD (25.0)

DroneManager::DroneManager()
{
    currentState = new Landed(this);
    droneManagerCurrentStatusMsg.status = droneMsgsROS::droneManagerStatus::LANDED;
    droneManagerPreviousStatusMsg.status = droneMsgsROS::droneManagerStatus::LANDED;
    tracker_object_on_frame = false;

//    DroneModuleInterface* dmi;
//    dmi = new DroneModuleInterface(std::string(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR));
//    drone_modules.Add(dmi);

}

DroneManager::~DroneManager()
{

}

void DroneManager::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg)
{
    last_battery_msg = (*msg);
}


void DroneManager::droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg)
{
    current_drone_position_reference = (*msg);
}

void DroneManager::estimatedPoseSubCallback(const droneMsgsROS::dronePose::ConstPtr& msg)
{
    last_estimatedPose = (*msg);
    return;
}


void DroneManager::droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
    last_navdata_timestamp = ros::Time::now();
    last_rotation_angles_msg = (msg);
}


void DroneManager::droneStatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg)
{
    // See comment on ardrone_autonomy/msg/Navdata.msg
    // # 0: Unknown, 1: Init, 2: Landed, 3: Flying, 4: Hovering, 5: Test
    // # 6: Taking off, 7: Goto Fix Point, 8: Landing, 9: Looping
    // # Note: 3,7 seems to discriminate type of flying (isFly = 3 | 7)


//    last_drone_status_msg = (*msg);
//    switch(last_drone_status_msg.status)
//    {
//        case droneMsgsROS::droneStatus::HOVERING:
//            cout<<"MidLevel in HOVERING"<<endl;
//            hover_from_altitud();
//            break;

//        case droneMsgsROS::droneStatus::LANDED:
//            cout<<"MidLevel in LANDED"<<endl;
//            land_from_altitud();
//            break;

//    }

    current_drone_status_msg = (*msg);
    switch(current_drone_status_msg.status)
    {
        case droneMsgsROS::droneStatus::HOVERING:
            if(last_drone_status_msg.status != droneMsgsROS::droneStatus::HOVERING)
            {
                cout<<"MidLevel in HOVERING"<<endl;
                hover_from_altitud();
            }
            break;

        case droneMsgsROS::droneStatus::LANDED:
            if(last_drone_status_msg.status != droneMsgsROS::droneStatus::LANDED)
            {
                cout<<"MidLevel in LANDED"<<endl;
                land_from_altitud();
            }
            break;

    }

    last_drone_status_msg = current_drone_status_msg;


}


void DroneManager::droneMPCommandCallback(const droneMsgsROS::droneMissionPlannerCommand::ConstPtr& msg)
{
    drone_modules_names.clear();
    last_MP_command     = msg->mpCommand;
    drone_modules_names = msg->drone_modules_names;
    received_MP_command = true;
    flag_command_received = true;


    switch(last_MP_command)
    {
        case droneMsgsROS::droneMissionPlannerCommand::HOVER:
            hover();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::SLEEP:
            sleep();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::LAND:
            land();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::LAND_AUTONOMOUS:
            land_autonomous();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF:
            takeoff();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::MOVE_MANUAL_ALTITUD:
            move_manual_altitud();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION:
            move_position();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::MOVE_SPEED:
            move_speed();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY:
            move_trajectory();
            break;

        case droneMsgsROS::droneMissionPlannerCommand::MOVE_VISUAL_SERVOING:
        {
            move_visual_servoing();

            std::vector<std::string> names;
            names = currentState->getNameOfBasicModules();
            cout<<"*****************************"<<endl;
            cout<<"MOVING VISUAL SERVOING MODULES (Constructor)"<<endl;
            for(int i=0;i<names.size();i++)
                cout<<names[i]<<endl;
            cout<<"*****************************"<<endl;
            break;
        }

        case droneMsgsROS::droneMissionPlannerCommand::MOVE_VISUAL_SERVOING_RL:
        {
            move_visual_servoing_rl();
            break;
        }
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP:
            loop();
            break;
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_BACK:
            loop();
            break;
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_FRONT:
            loop();
            break;
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_LEFT:
            loop();
            break;
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT:
            loop();
            break;

    }


}

void DroneManager::isObjectOnFrameCallback(const std_msgs::Bool &msg)
{
    tracker_object_on_frame = msg.data;

    switch(droneManagerCurrentStatusMsg.status)
    {
        case droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING:
            if(!tracker_object_on_frame && !flag_command_received)
                hover_visual_servoing();
            break;
        case droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING:
            if(tracker_object_on_frame)
                move_visual_servoing();
            break;
    }

    return;
}

void DroneManager::droneManagerStateDoContinueCallback(const std_msgs::Empty &msg)
{
    currentState->do_continue();
}

void DroneManager::wifiConnectionCallback(const std_msgs::Bool &msg)
{
    wifi_connection_status = msg.data;
    cout<<"WIFI Connection: "<<wifi_connection_status<<endl;
    //if(!wifi_connection_status)
    //{
    //    emergency();
    //}
    //else
      //  hover();
}



void DroneManager::open(ros::NodeHandle & nIn)
{
    //DroneModule::open(nIn);
    n = nIn;

    //drone_basic_modules.Open(n);
    drone_modules.Open(n);


    droneCommandPubl = n.advertise<droneMsgsROS::droneCommand>("command/high_level",1);
    droneMPCommandAckPubl = n.advertise<droneMsgsROS::droneHLCommandAck>("droneMissionHLCommandAck", 1);
    dronePositionRefsPub   = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("dronePositionRefs", 1);
    droneYawRefCommandPub  = n.advertise<droneMsgsROS::droneYawRefCommand>("droneControllerYawRefCommand", 1);
    droneDAltitudeCmdPubl = n.advertise<droneMsgsROS::droneDAltitudeCmd>("command/dAltitude",1);
    droneDYawCmdPubl = n.advertise<droneMsgsROS::droneDYawCmd>("command/dYaw",1);
    dronePitchRollCmdPubl = n.advertise<droneMsgsROS::dronePitchRollCmd>("command/pitch_roll",1);
    //droneStatusToMPPubl = n.advertise<droneMsgsROS::droneStatus>("droneStatusToMissionPlanner", 1, true);
    droneManagerStatusPubl = n.advertise<droneMsgsROS::droneManagerStatus>("droneManagerStatus", 1, true);
    droneManagerDoContinuePubl = n.advertise<std_msgs::Empty>("droneManagerStateDoContinue", 1, true);


    droneManagerDoContinueSubs = n.subscribe("droneManagerStateDoContinue", 1, &DroneManager::droneManagerStateDoContinueCallback, this);
    droneMPCommandSubs = n.subscribe("droneMissionPlannerCommand", 1,  &DroneManager::droneMPCommandCallback, this);
    drone_status_subscriber = n.subscribe("status", 1, &DroneManager::droneStatusCallback, this);
    drone_rotation_angles_subscriber = n.subscribe("rotation_angles", 1, &DroneManager::droneRotationAnglesCallback, this);
    //estimatedPoseSub = n.subscribe("ArucoSlam_EstimatedPose", 1, &DroneManager::estimatedPoseSubCallback, this);
    estimatedPoseSub = n.subscribe("EstimatedPose_droneGMR_wrt_GFF", 1, &DroneManager::estimatedPoseSubCallback, this);
    battery_level_subscriber = n.subscribe("battery", 1, &DroneManager::batteryCallback, this);
    is_object_on_frame_sub = n.subscribe("is_object_on_frame",1,&DroneManager::isObjectOnFrameCallback, this);
    wifi_connection_subscriber = n.subscribe("wifiIsOk", 10, &DroneManager::wifiConnectionCallback, this);
    getFligtAnimationClientsrv = n.serviceClient<droneMsgsROS::getFlightAnim>("ardrone/getflightanimation");


    batteryClientSrv = n.serviceClient<std_srvs::Trigger>("batteryIsOk");
    wifiClientSrv = n.serviceClient<std_srvs::Trigger>("wifiIsOk");
    moduleIsStartedClientSrv = n.serviceClient<droneMsgsROS::askForModule>("moduleIsStarted");
    moduleIsOnlineClientSrv = n.serviceClient<droneMsgsROS::askForModule>("moduleIsOnline");
    setInitDroneYaw_srv_server = n.serviceClient<droneMsgsROS::setInitDroneYaw_srv_type>(std::string("droneOdometryStateEstimator/setInitDroneYaw"));
    setControlModeClientSrv = n.serviceClient<droneMsgsROS::setControlMode>(std::string("droneTrajectoryController/setControlMode"));
}



void DroneManager::setCurrentState(ManagerState* state)
{
    currentState = state;
}

bool DroneManager::land()
{
    bool ack = currentState->land();
    return ack;
}

bool DroneManager::land_autonomous()
{
    bool ack = currentState->land_autonomous();
    return ack;
}

bool DroneManager::takeoff()
{
    cout<<"command for take off"<<endl;
    bool ack = currentState->takeoff();
    return ack;
}

bool DroneManager::hover()
{
    bool ack = currentState->hover();
    return ack;
}

bool DroneManager::sleep()
{
    bool ack = currentState->sleep();
    return ack;
}

bool DroneManager::move_manual_altitud()
{
    bool ack = currentState->move_manual_altitude();
    return ack;
}

bool DroneManager::move_position()
{
    bool ack = currentState->move_position();
    return ack;
}

bool DroneManager::move_speed()
{
    bool ack = currentState->move_speed();
    return ack;
}

bool DroneManager::move_trajectory()
{
    bool ack = currentState->move_trajectory();
    return ack;
}

bool DroneManager::move_visual_servoing()
{
    bool ack = currentState->move_visual_servoing();
    return ack;
}

bool DroneManager::move_visual_servoing_rl()
{
    bool ack = currentState->move_visual_servoing_rl();
    return ack;
}

bool DroneManager::hover_visual_servoing()
{
    bool ack = currentState->hover_visual_servoing();
    return ack;
}

bool DroneManager::hover_from_altitud()
{
    bool ack = currentState->hover_from_altitud();
    return ack;
}

bool DroneManager::land_from_altitud()
{
    bool ack = currentState->land_from_altitud();
    return ack;
}


bool DroneManager::loop()
{
    bool ack = currentState->loop();
    return ack;
}

bool DroneManager::emergency()
{
    bool ack = currentState->emergency();
    return ack;
}

bool DroneManager::do_continue()
{
    bool ack = currentState->do_continue();
    return ack;
}



void DroneManager::drone_takeOff()
{
    droneMsgsROS::droneCommand droneCommandMsgs;
    droneCommandMsgs.command = droneCommandMsgs.TAKE_OFF;
    droneCommandPubl.publish(droneCommandMsgs);
}

void DroneManager::drone_land()
{
    droneMsgsROS::droneCommand droneCommandMsgs;
    droneCommandMsgs.command = droneCommandMsgs.LAND;
    droneCommandPubl.publish(droneCommandMsgs);
}

void DroneManager::drone_hover()
{
    droneMsgsROS::droneCommand droneCommandMsgs;
    droneCommandMsgs.command = droneCommandMsgs.HOVER;
    droneCommandPubl.publish(droneCommandMsgs);
}

void DroneManager::drone_move()
{
    droneMsgsROS::droneCommand droneCommandMsgs;
    droneCommandMsgs.command = droneCommandMsgs.MOVE;
    droneCommandPubl.publish(droneCommandMsgs);
}

void DroneManager::drone_loop()
{
    //TODO: Send the command for Looping
    droneMsgsROS::getFlightAnim getFlightAnimation;

    switch(last_MP_command)
    {
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT:
            getFlightAnimation.request.AnimationMode.command = droneMsgsROS::FlightAnimation::ARDRONE_ANIM_FLIP_RIGHT;
            break;
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_LEFT:
            getFlightAnimation.request.AnimationMode.command = droneMsgsROS::FlightAnimation::ARDRONE_ANIM_FLIP_LEFT;
            break;
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_FRONT:
            getFlightAnimation.request.AnimationMode.command = droneMsgsROS::FlightAnimation::ARDRONE_ANIM_FLIP_AHEAD;
            break;
        case droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_BACK:
            getFlightAnimation.request.AnimationMode.command = droneMsgsROS::FlightAnimation::ARDRONE_ANIM_FLIP_BEHIND;
            break;
    }

    //use service
    getFligtAnimationClientsrv.call(getFlightAnimation);
}

void DroneManager::drone_emergency()
{
    droneMsgsROS::droneCommand droneCommandMsgs;
    droneCommandMsgs.command = droneCommandMsgs.EMERGENCY_STOP;
    droneCommandPubl.publish(droneCommandMsgs);
}

void DroneManager::publishDroneMPCommandAck(bool ack)
{
    droneMsgsROS::droneHLCommandAck msg;
    msg.time = ros::Time::now();
    msg.ack  = ack;
    droneMPCommandAckPubl.publish(msg);
    return;
}

void DroneManager::publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::_status_type droneManagerStatus)
{
    droneManagerCurrentStatusMsg.status = droneManagerStatus;
    droneManagerStatusPubl.publish(droneManagerCurrentStatusMsg);
}

void DroneManager::setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::_status_type droneManagerStatus)
{
    droneManagerPreviousStatusMsg.status = droneManagerStatus;
#ifdef DEBUG_MODE
    cout<<"In setManagerPreviousStatus"<<endl;
    switch(droneManagerPreviousStatusMsg.status)
    {
        case droneMsgsROS::droneManagerStatus::HOVERING:
            cout<<"Previous Manager Status: HOVERING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::SLEEPING:
            cout<<"Previous Manager Status: SLEEPING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING:
            cout<<"Previous Manager Status: HOVERING VISUAL SERVOING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDING:
            cout<<"Previous Manager Status: LANDING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDING_AUTONOMOUS:
            cout<<"Previous Manager Status: LANDING AUTONOMOUS"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDED:
            cout<<"Previous Manager Status: LANDED"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::TAKINGOFF:
            cout<<"Previous Manager Status: TAKINGOFF"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD:
            cout<<"Previous Manager Status: MOVING MANUAL ALTITUD"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_POSITION:
            cout<<"Previous Manager Status: MOVING POSITION"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_SPEED:
            cout<<"Previous Manager Status: MOVING SPEED"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY:
            cout<<"Previous Manager Status: MOVING TRAJECTORY"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING:
            cout<<"Previous Manager Status: MOVING VISUAL SERVOING"<<endl;
            break;

    }
#endif
}

void DroneManager::publishDroneManagerStatusDoContinue()
{
    std_msgs::Empty msg;
    ros::Duration(0.05).sleep();
    droneManagerDoContinuePubl.publish(msg);
}


void DroneManager::sendCurrentPositionAsPositionRef()
{
    // Note: p_this_drone_interface->last_estimatedPose is in droneGMR  wrt GFF
    // Note: position_ref                               is in droneLMrT wrt LMrTFF
    droneMsgsROS::dronePositionRefCommandStamped position_ref;
    droneMsgsROS::askForModule srv;
    position_ref.header.stamp       = ros::Time::now();
    position_ref.position_command.x = last_estimatedPose.x;
    position_ref.position_command.y = last_estimatedPose.y;
    position_ref.position_command.z = last_estimatedPose.z;
    droneMsgsROS::droneYawRefCommand yaw_ref;
    yaw_ref.header.stamp = ros::Time::now();
    yaw_ref.yaw          = last_estimatedPose.yaw;

    dronePositionRefsPub.publish(position_ref);
    droneYawRefCommandPub.publish(yaw_ref);

//    srv.request.module_name = MODULE_NAME_YAW_PLANNER;

//    if(moduleIsStartedClientSrv.call(srv))
//    {
//        if(!srv.response.ack)
//            droneYawRefCommandPub.publish(yaw_ref);
//    }

}

void DroneManager::clearCmd()
{
    //DronePitchRollCmd
    dronePitchRollCmdMsgs.pitchCmd = 0.0;
    dronePitchRollCmdMsgs.rollCmd = 0.0;
    droneDAltitudeCmdMsgs.dAltitudeCmd = 0.0;
    droneDYawCmdMsgs.dYawCmd = 0.0;

    droneDAltitudeCmdPubl.publish(droneDAltitudeCmdMsgs);
    droneDYawCmdPubl.publish(droneDYawCmdMsgs);
    dronePitchRollCmdPubl.publish(dronePitchRollCmdMsgs);

    return;
}

bool DroneManager::batteryCheckIsOk()
{
    return last_battery_msg.batteryPercent > BATTERY_LEVEL_CHECK_THRESHOLD;
}

bool DroneManager::manageModules(std::vector<std::string> &sv)
{
    bool module_integrated = false;

    std::vector<std::string> name_of_basic_modules = currentState->getNameOfBasicModules();
    for(int i=0;i<name_of_basic_modules.size();i++)
    {
        bool name_basic_module_found = false;
        for(int j=0;j<sv.size();j++)
        {
            if(name_of_basic_modules[i] == sv[j])
            {
                name_basic_module_found = true;
                break;
            }
        }
        if(!name_basic_module_found)
            sv.push_back(name_of_basic_modules[i]);
    }

#ifdef DEBUG_MODE
    cout<<"*** MODULES in MODULES_NAMES ***"<<endl;
    for(int i=0;i<sv.size();i++)
        cout<<sv[i]<<endl;
    cout<<"*** end MODULES NAMES ***"<<endl;
#endif

    //Names of the Modules that are in DroneModuleInterfaceList
    std::vector<std::string> previous_modules_names = drone_modules.GetNameOfDroneModules();
    for(int i=0;i<previous_modules_names.size();i++)
    {
        bool module_found = false;
        for(int j=0;j<sv.size();j++)
        {
            if(previous_modules_names[i] == sv[j])
            {
                module_found = true;
                break;
            }
        }
        if(!module_found)
            stopModule(previous_modules_names[i]);
    }

    for(int i=0;i<sv.size();i++)
    {
        if(addModule(sv[i]))
        {
            module_integrated = true;
            drone_modules.openModule(n,sv[i]);
        }
        //drone_modules.startModule(sv[i])
        if(drone_modules.startModule(sv[i]))
        {
            if(sv[i] == MODULE_NAME_ODOMETRY_STATE_ESTIMATOR)
                configureOdometryStateEstimator();
        }

    }


    return module_integrated;

}

void DroneManager::printModulesInList()
{
    cout<<"*** MODULES IN LIST ***"<<endl;
    for(int i=0;i<drone_modules.GetNameOfDroneModules().size();i++)
        cout<<drone_modules.GetNameOfDroneModules()[i]<<endl;
    cout<<"********************** END ***************"<<endl<<endl;
}

bool DroneManager::manageDroneModuleDrivers()
{
    bool all_drone_modules_drivers_integrated = false;
    int counter_drone_modules_drivers_started = 0;

    droneMsgsROS::askForModule srv;
    for(int i=0;i<drone_modules_drivers_names.size();i++)
    {
        if(addDriverModule(drone_modules_drivers_names[i]))
        {
            srv.request.module_name = drone_modules_drivers_names[i];
            moduleIsStartedClientSrv.call(srv);

            if(srv.response.ack)
            {
                cout<<"Supervisor: Driver Module "<<drone_modules_drivers_names[i]<<" Started"<<endl;
                counter_drone_modules_drivers_started++;
            }
            else
            {
                cout<<"Supervisor: Driver Module NOT STARTED"<<endl;
                startDriverModule(drone_modules_drivers_names[i]);
            }
        }
    }
    if(counter_drone_modules_drivers_started == drone_modules_drivers_names.size())
        all_drone_modules_drivers_integrated = true;

    return all_drone_modules_drivers_integrated;

}



bool DroneManager::addModule(std::string s)
{
    if(drone_modules.Add(s))
    {
        cout<<"Basic Module added..."<<endl;
        drone_modules.openModule(n,s);
        return true;
    }
    else
        return false;
}

bool DroneManager::startModule(std::string s)
{
    if(drone_modules.startModule(s))
        return true;
    else
        return false;
}

bool DroneManager::stopModule(std::string s)
{
    if(drone_modules.stopModule(s))
        return true;
    else
        return false;
}

bool DroneManager::addDriverModule(std::string s)
{
    if(drone_modules_drivers.Add(s))
    {
        cout<<"Driver Module added..."<<endl;
        drone_modules_drivers.openModule(n,s);
        return true;
    }
    else
        return false;
}

bool DroneManager::startDriverModule(std::string s)
{
    if(drone_modules_drivers.startModule(s))
        return true;
    else
        return false;
}

bool DroneManager::stopDriverModule(std::string s)
{
    if(drone_modules_drivers.stopModule(s))
        return true;
    else
        return false;
}

void DroneManager::addModulesNames(std::vector<std::string> sv)
{
    for(int i=0;i<sv.size();i++)
    {
        cout<<"adding module name..."<<endl;
        cout<<sv[i]<<endl;
        drone_modules_names.push_back(sv[i]);
    }
}



void DroneManager::init(std::string configFile)
{
    readDroneModuleDrivers(configFile);
    manageDroneModuleDrivers();

    return;
}


void DroneManager::readDroneModuleDrivers(std::string configFile)
{
    drone_modules_drivers_names.resize(0);

    //std::cout<<"file: "<<configFile<<std::endl;

    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if (!result) //FIXME, pass as argument
    {
        cout << "Xml file missing " << endl;
        assert(0);
    }


    // VisualMarkerList
    pugi::xml_node DroneModuleDrivers = doc.child("main").child("drone_module_drivers");

    // VisualMarkersRange
    for (pugi::xml_node DroneModule = DroneModuleDrivers.child("drone_module"); DroneModule; DroneModule = DroneModule.next_sibling("drone_module"))
    {
        drone_modules_drivers_names.push_back(DroneModule.child_value());
    }

//    for(unsigned int i=0;i<drone_modules_drivers_names.size();i++)
//        std::cout<<drone_modules_drivers_names[i]<<std::endl;


    return;
}

void DroneManager::configureOdometryStateEstimator()
{
    setInitDroneYaw_srv_var.request.yaw_droneLMrT_telemetry_rad = (last_rotation_angles_msg.vector.z)*(M_PI/180.0);
    setInitDroneYaw_srv_server.call(setInitDroneYaw_srv_var);
}










