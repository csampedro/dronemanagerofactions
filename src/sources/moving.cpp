#include "moving.h"
#include "manager_inc.h"


using namespace std;


Moving::Moving(DroneManager *manager_mach): drone_manager(manager_mach)
{
}

//Moving::Moving()
//{
//}


Moving::~Moving(void)
{

}

bool Moving::land()
{
    cout<<"From MOVING to LANDING"<<endl;

    drone_manager->setCurrentState(new Landing(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Moving::land_autonomous()
{
    cout<<"From MOVING to LANDING AUTONOMOUS"<<endl;

    drone_manager->setCurrentState(new LandingAutonomous(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDING_AUTONOMOUS);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Moving::hover()
{
    cout<<"From MOVING to HOVER"<<endl;

    drone_manager->setCurrentState(new Hovering(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::HOVERING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Moving::sleep()
{
    cout<<"From MOVING to SLEEP"<<endl;

    drone_manager->setCurrentState(new Sleeping(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::SLEEPING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool Moving::loop()
{
    cout<<"From MOVING to LOOPING"<<endl;

    drone_manager->setCurrentState(new Looping(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_FLIP);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Moving::move_manual_altitude()
{
    Moving* moving_state = new MovingManualAltitud(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Moving::move_position()
{
    Moving* moving_state = new MovingPosition(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_POSITION);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}
bool Moving::move_speed()
{
    Moving* moving_state = new MovingSpeed(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_SPEED);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}
bool Moving::move_trajectory()
{
    Moving* moving_state = new MovingTrajectory(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Moving::move_visual_servoing()
{
    Moving* moving_state = new MovingVisualServoing(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Moving::move_visual_servoing_rl()
{
    Moving* moving_state = new MovingVisualServoingRl(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVE_VISUAL_SERVOING_RL);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Moving::emergency()
{
    cout<<"From MOVING to EMERGENCY"<<endl;

    drone_manager ->setCurrentState(new Emergency(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::EMERGENCY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

//bool Moving::do_continue()
//{
//    cout<<"MOVING Sequence"<<endl;
//}









