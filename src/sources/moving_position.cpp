#include "moving_position.h"
#include "manager_inc.h"

//#define DEBUG_MODE

using namespace std;


MovingPosition::MovingPosition(DroneManager *manager_mach): Moving(manager_mach)
{
    state_step = 0;
    basic_modules_names.clear();
    //basic_modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    basic_modules_names.push_back(MODULE_NAME_TRAJECTORY_CONTROLLER);
}

MovingPosition::~MovingPosition(void)
{

}

//bool MovingPosition::move_position()
//{
//    cout<<"Already in MOVING in POSITION"<<endl;
//}


bool MovingPosition::do_continue()
{
#ifdef DEBUG_MODE
    cout<<"MOVING in POSITION Sequence"<<endl;
#endif
    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_basic_modules_started = 0;
    int counter_optional_modules_started = 0;
    //drone_manager->drone_move();

    switch(state_step)
    {
        case 0:
            //drone_manager->sendCurrentPositionAsPositionRef();
            drone_manager->addModule(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
            drone_manager->addModule(MODULE_NAME_TRAJECTORY_CONTROLLER);


            state_step++;

#ifdef DEBUG_MODE
            cout<<"MOVING POSITION Sequence-Step 0 finished"<<endl;
#endif

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_basic_modules_started = 0;
            for(int i=0;i<basic_modules_names.size();i++)
            {
                //srv.request.module_name = MODULE_NAME_TRAJECTORY_CONTROLLER;
                srv.request.module_name = basic_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module "<<basic_modules_names[i]<<" Started"<<endl;
#endif
                    counter_basic_modules_started++;
                }
                else
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module NOT STARTED"<<endl;
#endif
                    drone_manager->startModule(basic_modules_names[i]);
                }
            }
            if(counter_basic_modules_started == basic_modules_names.size())
            {
#ifdef DEBUG_MODE
                cout<<"MOVING POSITION Sequence-Step 1 finished"<<endl;
#endif
                //cout<<"Supervisor: You can proceed!"<<endl;
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
        {
            bool result = false;
            droneMsgsROS::setControlMode setControlModeSrv;
            setControlModeSrv.request.controlMode.command = Controller_MidLevel_controlMode::POSITION_CONTROL;
            //use service
            if (drone_manager->getControlModeClientSrv().call(setControlModeSrv))
                result = setControlModeSrv.response.ack;

            else
                result = false;

            //bool result = true;
            if(result)
            {
                drone_manager->sendCurrentPositionAsPositionRef();
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_POSITION);
//                drone_manager->publishDroneMPCommandAck(true);
                state_step++;
#ifdef DEBUG_MODE
                cout<<"MOVING POSITION Sequence-Step 2 finished"<<endl;
#endif
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;
        }

        case 3:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
#ifdef DEBUG_MODE
            cout<<"MOVING POSITION Sequence-Step 3 finished"<<endl;
#endif
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 4:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                //srv.request.module_name = MODULE_NAME_TRAJECTORY_CONTROLLER;
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
#endif
                    counter_optional_modules_started++;
                }
                else
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" NOT STARTED"<<endl;
#endif
                    drone_manager->startModule(optional_modules_names[i]);
                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_POSITION);
//                drone_manager->publishDroneMPCommandAck(true);
//                cout<<"Supervisor: You can proceed!"<<endl;
#ifdef DEBUG_MODE
                cout<<"MOVING POSITION Sequence-Step 4 finished"<<endl;
#endif
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 5:
            drone_manager->drone_move();
            if(drone_manager->getFlagCommandReceived())
            {
                drone_manager->publishDroneMPCommandAck(true);
                drone_manager->setFlagCommandReceived(false);
            }
#ifdef DEBUG_MODE
            cout<<"Supervisor: You can proceed!"<<endl;
#endif
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_POSITION);
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

    }



}










