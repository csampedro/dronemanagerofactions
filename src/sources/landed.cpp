#include "landed.h"
#include "manager_inc.h"


using namespace std;


Landed::Landed(DroneManager *manager_mach): drone_manager(manager_mach)
{
}

Landed::~Landed(void)
{

}

bool Landed::land()
{
    cout<<"Alredy in LANDED mode"<<endl;
    return false;
}


bool Landed::takeoff()
{
    cout<<"From LANDED to TAKINGOFF"<<endl;
    //drone_manager->drone_takeOff();

    drone_manager->setCurrentState(new Takingoff(drone_manager));
    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::TAKINGOFF);

    drone_manager->getCurrentState()->do_continue();

//    if(drone_manager->getDroneStatus().status==DroneState::TAKING_OFF)
//        drone_manager->setCurrentState(new Takingoff(drone_manager));



    delete this;
    return true;
}



bool Landed::do_continue()
{
    cout<<"LANDED Sequence"<<endl;
    drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::LANDED);
}








