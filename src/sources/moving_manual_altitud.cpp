#include "moving_manual_altitud.h"
#include "manager_inc.h"


using namespace std;


MovingManualAltitud::MovingManualAltitud(DroneManager *manager_mach): Moving(manager_mach)
{
    state_step = 0;
}

MovingManualAltitud::~MovingManualAltitud(void)
{

}


bool MovingManualAltitud::move_manual_altitude()
{
    cout<<"Alredy in MOVING MANUAL ALTITUD"<<endl;
}

bool MovingManualAltitud::do_continue()
{
    cout<<"MOVING MANUAL ALTITUD Sequence"<<endl;
    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_optional_modules_started = 0;

//    drone_manager->drone_move();
//    drone_manager->clearCmd();

    switch(state_step)
    {
        case 0:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
            cout<<"MOVING MANUAL ALTITUD Sequence-Step 0 finished"<<endl;
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
                    counter_optional_modules_started++;
                }
                else
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" NOT STARTED"<<endl;
                    drone_manager->startModule(optional_modules_names[i]);
                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
//                drone_manager->publishDroneMPCommandAck(true);
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD);
//                cout<<"Supervisor: You can proceed!"<<endl;
                cout<<"MOVING MANUAL ALTITUD Sequence-Step 1 finished"<<endl;
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
            drone_manager->clearCmd();
            drone_manager->drone_move();
            if(drone_manager->getFlagCommandReceived())
            {
                drone_manager->publishDroneMPCommandAck(true);
                drone_manager->setFlagCommandReceived(false);
            }
            cout<<"Supervisor: You can proceed!"<<endl;
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD);
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;


    }



}









