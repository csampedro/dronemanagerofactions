#include "moving_trajectory.h"
#include "manager_inc.h"


using namespace std;


MovingTrajectory::MovingTrajectory(DroneManager *manager_mach): Moving(manager_mach)
{
    state_step = 0;
    basic_modules_names.clear();
    basic_modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
    basic_modules_names.push_back(MODULE_NAME_TRAJECTORY_CONTROLLER);
}

MovingTrajectory::~MovingTrajectory(void)
{

}



bool MovingTrajectory::do_continue()
{
    cout<<"MOVING TRAJECTORY Sequence"<<endl;
    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_basic_modules_started = 0;
    int counter_optional_modules_started = 0;
    //drone_manager->drone_move();

    switch(state_step)
    {
        case 0:
            //drone_manager->sendCurrentPositionAsPositionRef();
            drone_manager->addModule(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
            drone_manager->addModule(MODULE_NAME_TRAJECTORY_CONTROLLER);


            state_step++;
            cout<<"MOVING TRAJECTORY Sequence-Step 0 finished"<<endl;
            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_basic_modules_started = 0;
            for(int i=0;i<basic_modules_names.size();i++)
            {
                //srv.request.module_name = MODULE_NAME_TRAJECTORY_CONTROLLER;
                srv.request.module_name = basic_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
                    cout<<"Supervisor: Module "<<basic_modules_names[i]<<" Started"<<endl;
                    counter_basic_modules_started++;
                }
                else
                {
                    cout<<"Supervisor: Module "<<basic_modules_names[i]<<" NOT STARTED"<<endl;
                    if (basic_modules_names[i]==MODULE_NAME_TRAJECTORY_CONTROLLER)
                    {
                        drone_manager->sendCurrentPositionAsPositionRef();

                    cout<<"Sending sendCurrentPositionAsPositionRef"<<endl;
                    }

                    drone_manager->startModule(basic_modules_names[i]);
                    if (basic_modules_names[i]==MODULE_NAME_ODOMETRY_STATE_ESTIMATOR)
                        drone_manager->configureOdometryStateEstimator();

                }
            }
            if(counter_basic_modules_started == basic_modules_names.size())
            {
                cout<<"MOVING TRAJECTORY Sequence-Step 1 finished"<<endl;
                cout<<"Supervisor: You can proceed!"<<endl;
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
        {
//            bool result = false;
//            droneMsgsROS::setControlMode setControlModeSrv;
//            setControlModeSrv.request.controlMode.command = Controller_MidLevel_controlMode::TRAJECTORY_CONTROL;
//            //use service
//            if (drone_manager->getControlModeClientSrv().call(setControlModeSrv))
//            {
//                result = setControlModeSrv.response.ack;
//                cout<<"service call result: "<<result;
//            }

//            else
//                result = false;

            bool result = true;
            if(result)
            {
                cout<<"Case 2 "<<endl;
                //drone_manager->sendCurrentPositionAsPositionRef();
//                drone_manager->publishDroneMPCommandAck(true);
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY);
                ros::spinOnce();
                state_step++;
                cout<<"MOVING TRAJECTORY Sequence-Step 2 finished"<<endl;

            }
            drone_manager->publishDroneManagerStatusDoContinue();
            break;
        }

        case 3:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
            cout<<"MOVING TRAJECTORY Sequence-Step 3 finished"<<endl;
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 4:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                //srv.request.module_name = MODULE_NAME_TRAJECTORY_CONTROLLER;
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
                    counter_optional_modules_started++;
                }
                else
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" NOT STARTED"<<endl;
                    drone_manager->startModule(optional_modules_names[i]);
                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY);
//                drone_manager->publishDroneMPCommandAck(true);
//                cout<<"Supervisor: You can proceed!"<<endl;
                cout<<"MOVING TRAJECTORY Sequence-Step 4 finished"<<endl;
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;
        case 5:
            drone_manager->drone_move();
            if(drone_manager->getFlagCommandReceived())
            {
                drone_manager->publishDroneMPCommandAck(true);
                drone_manager->setFlagCommandReceived(false);
            }
            cout<<"Supervisor: You can proceed!"<<endl;
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY);
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;


    }
    //drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY);


}









