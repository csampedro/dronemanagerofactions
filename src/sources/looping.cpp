#include "looping.h"
#include "manager_inc.h"


using namespace std;


Looping::Looping(DroneManager *manager_mach): drone_manager(manager_mach)
{
    state_step = 0;
}

Looping::~Looping(void)
{

}

bool Looping::hover()
{
    cout<<"From LOOPING to HOVER"<<endl;

    drone_manager ->setCurrentState(new Hovering(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::HOVERING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Looping::hover_from_altitud()
{
    cout<<"From LOOPING to HOVER"<<endl;


    if(drone_manager->getDroneStatus().status == DroneState::HOVERING)
    {
        drone_manager ->setCurrentState(new Hovering(drone_manager));
        drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::HOVERING);
        drone_manager->getCurrentState()->do_continue();
    }


    delete this;
    return true;
}

bool Looping::do_continue()
{
    cout<<"Looping Sequence"<<endl;
    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_optional_modules_started = 0;
    //drone_manager->drone_loop();

    switch(state_step)
    {
        case 0:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
            cout<<"LOOPING Sequence-Step 0 finished"<<endl;
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
                    counter_optional_modules_started++;
                }
                else
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" NOT STARTED"<<endl;
                    drone_manager->startModule(optional_modules_names[i]);
                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
//                drone_manager->publishDroneMPCommandAck(true);
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_FLIP);
//                cout<<"Supervisor: You can proceed!"<<endl;
                cout<<"LOOPING Sequence-Step 1 finished"<<endl;
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
            drone_manager->drone_loop();
            if(drone_manager->getFlagCommandReceived())
            {
                drone_manager->publishDroneMPCommandAck(true);
                drone_manager->setFlagCommandReceived(false);
            }
            cout<<"Supervisor: You can proceed!"<<endl;
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::MOVING_FLIP);

            state_step++;
            drone_manager->publishDroneManagerStatusDoContinue();
            break;


    }

}












