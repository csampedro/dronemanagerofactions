#include "landing.h"
#include "manager_inc.h"


using namespace std;


Landing::Landing(DroneManager *manager_mach): drone_manager(manager_mach)
{
}

Landing::~Landing(void)
{

}

bool Landing::land()
{
    cout<<"Alredy in LANDING mode"<<endl;
    return false;
}

bool Landing::takeoff()
{
    cout<<"From LANDING to TAKINGOFF"<<endl;
    drone_manager->setCurrentState(new Takingoff(drone_manager));
    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::TAKINGOFF);

    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Landing::land_from_altitud()
{
    cout<<"From LANDING to LANDED"<<endl;
    drone_manager->setCurrentState(new Landed(drone_manager));
    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDED);

    drone_manager->getCurrentState()->do_continue();
}

bool Landing::do_continue()
{
    cout<<"LANDING Sequence"<<endl;
//    droneMsgsROS::askForModule srv;
//    srv.request.module_name = MODULE_NAME_TRAJECTORY_CONTROLLER;
//    if(drone_manager->getModuleIsStartedClientSrv().call(srv))

    drone_manager->stopModule(MODULE_NAME_TRAJECTORY_CONTROLLER);

    drone_manager->clearCmd();
    drone_manager->drone_land();
    if(drone_manager->getFlagCommandReceived())
    {
        drone_manager->publishDroneMPCommandAck(true);
        drone_manager->setFlagCommandReceived(false);
    }
    drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::LANDING);

}










