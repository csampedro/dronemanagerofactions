#include "sleeping.h"
#include "manager_inc.h"


using namespace std;


Sleeping::Sleeping(DroneManager *manager_mach) : drone_manager(manager_mach)
{
    state_step = 0;
}

Sleeping::~Sleeping(void)
{

}

bool Sleeping::land()
{
    cout<<"From SLEEPING to LANDING"<<endl;
    drone_manager->setCurrentState(new Landing(drone_manager));
    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDING);

    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Sleeping::land_autonomous()
{
    cout<<"From SLEEPING to LANDING AUTONOMOUS"<<endl;
    drone_manager->setCurrentState(new LandingAutonomous(drone_manager));
    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDING_AUTONOMOUS );

    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Sleeping::loop()
{
    cout<<"From SLEEPING to LOOPING"<<endl;

    drone_manager ->setCurrentState(new Looping(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_FLIP);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool Sleeping::sleep()
{
//    cout<<"Alredy in SLEEPING mode"<<endl;
//    return false;
    cout<<"From SLEEP to SLEEP"<<endl;

    drone_manager->setCurrentState(new Sleeping(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::SLEEPING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool Sleeping::move_manual_altitude()
{
    cout<<"From Sleeping to Moving MANUAL IN ALTITUD"<<endl;

    Moving* moving_state = new MovingManualAltitud(drone_manager);
    drone_manager->setCurrentState(moving_state);


    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Sleeping::move_position()
{
    cout<<"From Sleeping to Moving in POSITION"<<endl;

    Moving* moving_state = new MovingPosition(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_POSITION);
    drone_manager->getCurrentState()->do_continue();


    delete this;
    return true;
}

bool Sleeping::move_speed()
{
    cout<<"From Sleeping to Moving in SPEED"<<endl;

    Moving* moving_state = new MovingSpeed(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_SPEED);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}


bool Sleeping::move_trajectory()
{
    cout<<"From Sleeping to Moving in TRAJECTORY"<<endl;

    Moving* moving_state = new MovingTrajectory(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Sleeping::move_visual_servoing()
{
    cout<<"From Sleeping to Moving in VISUAL SERVOING"<<endl;

    Moving* moving_state = new MovingVisualServoing (drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Sleeping::move_visual_servoing_rl()
{
    cout<<"From Sleeping to Moving in VISUAL SERVOING with Reinforcement Learning"<<endl;

    Moving* moving_state = new MovingVisualServoingRl(drone_manager);
    drone_manager->setCurrentState(moving_state);

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::MOVE_VISUAL_SERVOING_RL);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Sleeping::emergency()
{
    cout<<"From SLEEPING to EMERGENCY"<<endl;

    drone_manager ->setCurrentState(new Emergency(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::EMERGENCY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Sleeping::do_continue()
{
#ifdef DEBUG_MODE
    cout<<"SLEEPING Sequence"<<endl;
#endif
    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_optional_modules_started = 0;


    switch(state_step)
    {
        case 0:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
#ifdef DEBUG_MODE
            cout<<"SLEEPING Sequence-Step 0 finished"<<endl;
#endif
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
#endif
                    counter_optional_modules_started++;
                }
                else
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" NOT STARTED"<<endl;
                    drone_manager->printModulesInList();
#endif
                    drone_manager->startModule(optional_modules_names[i]);


                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
#ifdef DEBUG_MODE
                cout<<"SLEEPING Sequence-Step 1 finished"<<endl;
#endif
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
            if(drone_manager->getFlagCommandReceived())
            {
                drone_manager->publishDroneMPCommandAck(true);
                drone_manager->setFlagCommandReceived(false);
            }
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::SLEEPING);
#ifdef DEBUG_MODE
            cout<<"Supervisor: You can proceed!"<<endl;
#endif
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;



    }


}







