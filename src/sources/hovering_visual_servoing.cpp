#include "hovering_visual_servoing.h"
#include "manager_inc.h"


using namespace std;


HoveringVisualServoing::HoveringVisualServoing(DroneManager *manager_mach): Hovering(manager_mach)
{
    state_step = 0;
    basic_modules_names.clear();
    basic_modules_names.push_back(MODUlE_NAME_OPENTLD_TRANSLATOR);
    basic_modules_names.push_back(MODULE_NAME_TRACKER_EYE);
    basic_modules_names.push_back(MODULE_NAME_IBVS_CONTROLLER);
}

HoveringVisualServoing::~HoveringVisualServoing(void)
{

}


bool HoveringVisualServoing::hover()
{
    cout<<"From HOVERING VISUAL SERVOING to HOVER"<<endl;

    drone_manager->setCurrentState(new Hovering(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::HOVERING);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return false;
}



bool HoveringVisualServoing::do_continue()
{
    cout<<"HOVERING VISUAL SERVOING Sequence"<<endl;
    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_optional_modules_started = 0;


//    drone_manager->drone_hover();
//    drone_manager->clearCmd();

    switch(state_step)
    {
        case 0:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
            cout<<"HOVERING VISUAL SERVOING Sequence-Step 0 finished"<<endl;
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
                    counter_optional_modules_started++;
                }
                else
                {
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" NOT STARTED"<<endl;
                    drone_manager->startModule(optional_modules_names[i]);
                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING);
//                cout<<"Supervisor: You can proceed!"<<endl;
                cout<<"HOVERING VISUAL SERVOING Sequence-Step 1 finished"<<endl;
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
            drone_manager->clearCmd();
            drone_manager->drone_hover();
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING);
            cout<<"Supervisor: You can proceed!"<<endl;
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

    }

}







