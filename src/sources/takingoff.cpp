#include "takingoff.h"
#include "manager_inc.h"


using namespace std;


Takingoff::Takingoff(DroneManager *manager_mach): drone_manager(manager_mach)
{
    state_step = 0;
}

Takingoff::~Takingoff(void)
{

}


bool Takingoff::takeoff()
{
    cout<<"Alredy in TAKINGOFF mode"<<endl;
    return false;
}

//bool Takingoff::hover()
//{
//    cout<<"From TAKINGOFF to HOVER"<<endl;

//    drone_manager ->setCurrentState(new Hovering(drone_manager));

//    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::HOVERING);
//    drone_manager->getCurrentState()->do_continue();

//    delete this;
//    return true;
//}

bool Takingoff::hover_from_altitud()
{
    cout<<"From TAKINGOFF to HOVER"<<endl;


    if(drone_manager->getDroneStatus().status == DroneState::HOVERING)
    {
        drone_manager->setCurrentState(new Hovering(drone_manager));
        drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::HOVERING);
        drone_manager->getCurrentState()->do_continue();
    }


    delete this;
    return true;
}

bool Takingoff::land()
{
    drone_manager->setCurrentState(new Landing(drone_manager));
    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::LANDING);
    drone_manager->getCurrentState()->do_continue();


    delete this;
    return true;
}

bool Takingoff::emergency()
{
    cout<<"From TAKINGOFF to EMERGENCY"<<endl;

    drone_manager ->setCurrentState(new Emergency(drone_manager));

    drone_manager->publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::EMERGENCY);
    drone_manager->getCurrentState()->do_continue();

    delete this;
    return true;
}

bool Takingoff::do_continue()
{
#ifdef DEBUG_MODE
    cout<<"TAKINGOFF Sequence"<<endl;
#endif
    droneMsgsROS::askForModule srv;
    std::vector<std::string> optional_modules_names;
    int counter_optional_modules_started = 0;

    //drone_manager->drone_takeOff();


    switch(state_step)
    {
        case 0:
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            drone_manager->manageModules(optional_modules_names);
#ifdef DEBUG_MODE
            cout<<"TAKINGOFF Sequence-Step 0 finished"<<endl;
#endif
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 1:
            counter_optional_modules_started = 0;
            optional_modules_names.clear();
            optional_modules_names = drone_manager->getNamesOfDroneOptionalModules();
            for(int i=0;i<optional_modules_names.size();i++)
            {
                srv.request.module_name = optional_modules_names[i];
                drone_manager->getModuleIsStartedClientSrv().call(srv);

                if(srv.response.ack)
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module "<<optional_modules_names[i]<<" Started"<<endl;
#endif
                    counter_optional_modules_started++;
                }
                else
                {
#ifdef DEBUG_MODE
                    cout<<"Supervisor: Module NOT STARTED"<<endl;
#endif
                    drone_manager->startModule(optional_modules_names[i]);
                }
            }
            if(counter_optional_modules_started == optional_modules_names.size())
            {
//                drone_manager->publishDroneMPCommandAck(true);
//                drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::TAKINGOFF);
//                cout<<"Supervisor: You can proceed!"<<endl;
#ifdef DEBUG_MODE
                cout<<"TAKINGOFF Sequence-Step 1 finished"<<endl;
#endif
                state_step++;
            }

            drone_manager->publishDroneManagerStatusDoContinue();
            break;

        case 2:
            drone_manager->drone_takeOff();
            if(drone_manager->getFlagCommandReceived())
            {
                drone_manager->publishDroneMPCommandAck(true);
                drone_manager->setFlagCommandReceived(false);
            }
#ifdef DEBUG_MODE
            cout<<"Supervisor: You can proceed!"<<endl;
#endif
            drone_manager->setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::TAKINGOFF);
            state_step++;

            drone_manager->publishDroneManagerStatusDoContinue();
            break;


    }

}












