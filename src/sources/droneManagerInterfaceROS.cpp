#include "droneManagerInterfaceROS.h"

using namespace std;


DroneManagerInterfaceROS::DroneManagerInterfaceROS(void)
{
}

DroneManagerInterfaceROS::~DroneManagerInterfaceROS(void)
{

}


void DroneManagerInterfaceROS::droneStatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg)
{
    // See comment on ardrone_autonomy/msg/Navdata.msg
    // # 0: Unknown, 1: Init, 2: Landed, 3: Flying, 4: Hovering, 5: Test
    // # 6: Taking off, 7: Goto Fix Point, 8: Landing, 9: Looping
    // # Note: 3,7 seems to discriminate type of flying (isFly = 3 | 7)
    last_drone_status_msg = (*msg);

}

void DroneManagerInterfaceROS::missionToManagerCallback(const std_msgs::String::ConstPtr& msg)
{
    ROS_INFO("I heard: [%s]", msg->data.c_str());


}


void DroneManagerInterfaceROS::open(ros::NodeHandle & nIn)
{
    //DroneModule::open(nIn);
    n = nIn;

    drone_status_subscriber=n.subscribe("drone1/status", 1, &DroneManagerInterfaceROS::droneStatusCallback, this);

    DroneCommandPubl=n.advertise<droneMsgsROS::droneCommand>("drone1/command/high_level",1, true);

    drone_action_suscriber = n.subscribe("missionPlanner_to_manager", 1000, &DroneManagerInterfaceROS::missionToManagerCallback, this);

}


void DroneManagerInterfaceROS::drone_takeOff()
{
    droneMsgsROS::droneCommand DroneCommandMsgs;
    DroneCommandMsgs.command = DroneCommandMsgs.TAKE_OFF;
    DroneCommandPubl.publish(DroneCommandMsgs);
}

void DroneManagerInterfaceROS::drone_land()
{
    droneMsgsROS::droneCommand DroneCommandMsgs;
    DroneCommandMsgs.command = DroneCommandMsgs.LAND;
    DroneCommandPubl.publish(DroneCommandMsgs);
}

void DroneManagerInterfaceROS::drone_hover()
{
    droneMsgsROS::droneCommand DroneCommandMsgs;
    DroneCommandMsgs.command = DroneCommandMsgs.HOVER;
    DroneCommandPubl.publish(DroneCommandMsgs);
}

void DroneManagerInterfaceROS::drone_move()
{
    droneMsgsROS::droneCommand DroneCommandMsgs;
    DroneCommandMsgs.command = DroneCommandMsgs.MOVE;
    DroneCommandPubl.publish(DroneCommandMsgs);
}







