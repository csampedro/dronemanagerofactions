//////////////////////////////////////////////////////
//  droneManager.h
//
//  Created on: Jul 15, 2015
//      Author: carlos sampedro
//
//  Last modification on: Jul 15, 2014
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef DRONE_MANAGER_H
#define DRONE_MANAGER_H


#include <iostream>
#include <string>
#include <vector>


// ROS
#include "ros/ros.h"
#include "droneManager.h"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "communication_definition.h"
#include "std_srvs/Trigger.h"

// Flying modes
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneManagerStatus.h"
#include "droneMsgsROS/droneCommand.h"
#include "droneMsgsROS/askForModule.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/setControlMode.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/getFlightAnim.h"
#include "droneMsgsROS/FlightAnimation.h"
#include "nodes_definition.h"
#include <droneMsgsROS/battery.h>
#include <thread>

// SetInitDroneYaw for Controller and StateEstimator
#include "droneMsgsROS/setInitDroneYaw_srv_type.h"
#include <geometry_msgs/Vector3Stamped.h>
#include <droneMsgsROS/droneAltitude.h>
#include <droneMsgsROS/vector2Stamped.h>
#include "control/Controller_MidLevel_controlModes.h"

#include "managerState.h"
#include "dronemoduleinterface.h"
#include "droneModuleInterfaceList.h"

#include "pugixml.hpp"


/////////////////////////////////////////
// Class DroneManager
//
//   Description
//   This class implements the manager
//   state machine
//
/////////////////////////////////////////
class DroneManager
{
private:
    ros::NodeHandle   n;
    ManagerState* currentState;
    DroneModuleInterfaceList drone_modules;
    DroneModuleInterfaceList drone_modules_drivers;


    droneMsgsROS::droneManagerStatus droneManagerCurrentStatusMsg;
    droneMsgsROS::droneManagerStatus droneManagerPreviousStatusMsg;
    droneMsgsROS::battery last_battery_msg;
    droneMsgsROS::dronePose   current_drone_position_reference;
    droneMsgsROS::droneStatus last_drone_status_msg;
    droneMsgsROS::droneStatus current_drone_status_msg;
    ros::Time last_navdata_timestamp;
    geometry_msgs::Vector3Stamped last_rotation_angles_msg;
    droneMsgsROS::dronePose last_estimatedPose;        // to have this_drone last estimated pose (from localizer)
//    droneMsgsROS::droneAltitude last_altitude_msg;
//    droneMsgsROS::vector2Stamped last_ground_optical_flow_msg;
    int32_t last_MP_command;
    std::vector<std::string> drone_modules_names;
    std::vector<std::string> drone_modules_drivers_names;
    bool received_MP_command;
    bool tracker_object_on_frame;
    bool wifi_connection_status;
    droneMsgsROS::dronePitchRollCmd dronePitchRollCmdMsgs;
    droneMsgsROS::droneDAltitudeCmd droneDAltitudeCmdMsgs;
    droneMsgsROS::droneDYawCmd droneDYawCmdMsgs;
    droneMsgsROS::droneCommand droneCommandMsgs;
    droneMsgsROS::setInitDroneYaw_srv_type setInitDroneYaw_srv_var;



    ros::Publisher droneCommandPubl;
    ros::Publisher droneMPCommandAckPubl;
    //ros::Publisher droneStatusToMPPubl;
    ros::Publisher dronePositionRefsPub;
    ros::Publisher droneYawRefCommandPub;
    ros::Publisher droneDAltitudeCmdPubl;
    ros::Publisher droneDYawCmdPubl;
    ros::Publisher dronePitchRollCmdPubl;
    ros::Publisher droneManagerStatusPubl;
    ros::Publisher droneManagerDoContinuePubl;


    ros::Subscriber drone_status_subscriber;
    ros::Subscriber droneMPCommandSubs;
    ros::Subscriber drone_rotation_angles_subscriber;
    ros::Subscriber estimatedPoseSub;
    ros::Subscriber battery_level_subscriber;
    ros::Subscriber is_object_on_frame_sub;
    ros::Subscriber droneManagerDoContinueSubs;
    ros::Subscriber wifi_connection_subscriber;
    ros::ServiceClient getFligtAnimationClientsrv;


    //Comunication with the Supervisor
    ros::ServiceClient batteryClientSrv;
    ros::ServiceClient wifiClientSrv;
    ros::ServiceClient moduleIsStartedClientSrv;
    ros::ServiceClient moduleIsOnlineClientSrv;
    ros::ServiceClient setInitDroneYaw_srv_server;
    ros::ServiceClient setControlModeClientSrv;

    bool flag_command_received;


public:
    DroneManager();
    ~DroneManager();
    void setCurrentState (ManagerState* state);

    ManagerState* getCurrentState(){return currentState;}
    //droneMsgsROS::droneStatus getDroneStatus(){return last_drone_status_msg;}
    droneMsgsROS::droneStatus getDroneStatus(){return current_drone_status_msg;}
    int32_t getLastMissionPlannerCommand(){return last_MP_command;}
    geometry_msgs::Vector3Stamped getLastRotationAnglesMsg(){return last_rotation_angles_msg;}
    ros::ServiceClient getInitDroneYawSrvServer(){return setInitDroneYaw_srv_server;}
    ros::ServiceClient getControlModeClientSrv(){return setControlModeClientSrv;}
    ros::ServiceClient getModuleIsStartedClientSrv(){return moduleIsStartedClientSrv;}
    ros::ServiceClient getModuleIsOnlineClientSrv(){return moduleIsOnlineClientSrv;}
    void setManagerPreviousStatus(droneMsgsROS::droneManagerStatus::_status_type droneManagerStatus);
    droneMsgsROS::droneManagerStatus getManagerPreviousStatus(){return droneManagerPreviousStatusMsg;}
    std::vector<std::string> getNamesOfDroneOptionalModules(){return drone_modules_names;}
    bool getFlagCommandReceived(){return flag_command_received;}
    void printModulesInList();



    void open(ros::NodeHandle & nIn);
    void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg);
    void droneStatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg);
    void droneMPCommandCallback(const droneMsgsROS::droneMissionPlannerCommand::ConstPtr& msg);
    void droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);
    void estimatedPoseSubCallback(const droneMsgsROS::dronePose::ConstPtr& msg);
    void droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg);
    void isObjectOnFrameCallback(const std_msgs::Bool &msg);
    void droneManagerStateDoContinueCallback(const std_msgs::Empty &msg);
    void wifiConnectionCallback(const std_msgs::Bool &msg);


    bool land();
    bool land_autonomous();
    bool takeoff();
    bool hover();
    bool move_manual_altitud();
    bool move_position();
    bool move_speed();
    bool move_trajectory();
    bool move_visual_servoing();
    bool move_visual_servoing_rl();
    bool hover_visual_servoing();
    bool hover_from_altitud();
    bool land_from_altitud();
    bool emergency();
    bool loop();
    bool sleep();
    bool do_continue();

    void drone_takeOff();
    void drone_land();
    void drone_hover();
    void drone_move();
    void drone_loop();
    void drone_emergency();

    void sendCurrentPositionAsPositionRef();
    void publishDroneMPCommandAck( bool ack);
    void publishDroneManagerStatus(droneMsgsROS::droneManagerStatus::_status_type droneManagerStatus);
    void publishDroneManagerStatusDoContinue();
    void clearCmd();
    bool batteryCheckIsOk();


    //bool manageOptionalModules(std::vector<std::string> sv);
    void addModulesNames(std::vector<std::string> sv);
    bool manageModules(std::vector<std::string> &sv);
    bool manageDroneModuleDrivers();
    bool addModule(std::string s);
    bool startModule(std::string s);
    bool stopModule(std::string s);
    bool addDriverModule(std::string s);
    bool startDriverModule(std::string s);
    bool stopDriverModule(std::string s);
    void init(std::string configFile);
    void readDroneModuleDrivers(std::string configFile);
    void configureOdometryStateEstimator();
    void setFlagCommandReceived(bool flag){flag_command_received = flag;}


};



#endif
