//////////////////////////////////////////////////////
//  landing_autonomous.h
//
//  Created on: Oct 1, 2016
//      Author: carlos sampedro
//
//  Last modification on: Oct 1, 2016
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef LANDING_AUTONOMOUS_H
#define LANDING_AUTONOMOUS_H


#include <iostream>
#include <string>
#include <vector>


#include "droneManager.h"


////////////////////////////////////////////
// Class LandingAutonomous
//
//   Description
//   This class implements the manager state: 
//   LandingAutonomous
//
////////////////////////////////////////////
class LandingAutonomous: public ManagerState
{
protected:
    DroneManager* drone_manager;
public:
    LandingAutonomous(DroneManager* manager_mach);
    ~LandingAutonomous(void);
    bool land();
    virtual bool land_autonomous();
    bool move_manual_altitude();
    bool move_position();
    bool move_speed();
    bool move_trajectory();
    bool move_visual_servoing();
    bool emergency();
    virtual bool do_continue();

};



#endif
