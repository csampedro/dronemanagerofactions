//////////////////////////////////////////////////////
//  managerState.h
//
//  Created on: Jul 15, 2015
//      Author: carlos sampedro
//
//  Last modification on: Jul 15, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MANAGER_STATE_H
#define MANAGER_STATE_H


#include <iostream>
#include <string>
#include <vector>


#include "opencv2/opencv.hpp"
#include "drone_utils/drone_state_enum.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/setInitDroneYaw_srv_type.h"
#include "droneMsgsROS/droneManagerStatus.h"


/////////////////////////////////////////
// Class ManagerState
//
//   Description
//   This class implements the Interface
//   between the diferent concrete states
//
/////////////////////////////////////////
class ManagerState
{
protected:
    int state_step;
    std::vector<std::string> basic_modules_names;

public:
    ManagerState(void);
    virtual ~ManagerState(void);
    virtual std::vector<std::string> getNameOfBasicModules(){return basic_modules_names;}
    virtual bool land();
    virtual bool takeoff();
    virtual bool hover();
    virtual bool loop();
    virtual bool hover_from_altitud();
    virtual bool land_from_altitud();
    virtual bool move_manual_altitude();
    virtual bool move_position();
    virtual bool move_speed();
    virtual bool move_trajectory();
    virtual bool move_visual_servoing();
    virtual bool move_visual_servoing_rl();
    virtual bool hover_visual_servoing();
    virtual bool emergency();
    virtual bool sleep();
    virtual bool land_autonomous();
    virtual bool do_continue()=0;


};



#endif
