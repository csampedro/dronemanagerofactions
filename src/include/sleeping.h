//////////////////////////////////////////////////////
//  sleeping.h
//
//  Created on: Sept 20, 2016
//      Author: carlos sampedro
//
//  Last modification on: Sept 20, 2016
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef SLEEPING_H
#define SLEEPING_H


#include <iostream>
#include <string>
#include <vector>


#include "droneManager.h"


////////////////////////////////////////////
// Class Sleeping
//
//   Description
//   This class implements the manager state: 
//   Sleeping
//
////////////////////////////////////////////
class Sleeping: public ManagerState
{
protected:
    DroneManager* drone_manager;
public:
    Sleeping(DroneManager* manager_mach);
    ~Sleeping(void);
    bool land();
    bool land_autonomous();
    bool loop();
    virtual bool sleep();
    bool move_manual_altitude();
    bool move_position();
    bool move_speed();
    bool move_trajectory();
    bool move_visual_servoing();
    bool move_visual_servoing_rl();
    bool emergency();
    virtual bool do_continue();

};



#endif
