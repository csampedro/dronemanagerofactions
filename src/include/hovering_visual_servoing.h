//////////////////////////////////////////////////////
//  hovering_visual_servoing.h
//
//  Created on: Sept 7, 2015
//      Author: carlos sampedro
//
//  Last modification on: Sept 7, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef HOVERING_VISUAL_SERVOING_H
#define HOVERING_VISUAL_SERVOING_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"
#include "hovering.h"


////////////////////////////////////////////
// Class HoveringVisualServoing
//
//   Description
//   This class implements the manager state: 
//   HoveringVisualServoing
//
////////////////////////////////////////////
class HoveringVisualServoing : public Hovering
{
//private:
//    DroneManager* drone_manager;
public:
    HoveringVisualServoing(DroneManager *manager_mach);
    ~HoveringVisualServoing(void);
//    bool land();
//    bool move_position();
//    bool move_visual_servoing();
    bool hover();
    bool do_continue();

};



#endif
