//////////////////////////////////////////////////////
//  moving_manual_thrust.h
//
//  Created on: Aug 5, 2015
//      Author: carlos sampedro
//
//  Last modification on: Aug 5, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MOVING_MANUAL_THRUST_H
#define MOVING_MANUAL_THRUST_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"
#include "moving.h"


////////////////////////////////////////////
// Class MovingManualThrust
//
//   Description
//   This class implements the manager state: 
//   MovingManualThrust
//
////////////////////////////////////////////
class MovingManualThrust : public Moving
{

public:
    MovingManualThrust(DroneManager *manager_mach);
    ~MovingManualThrust(void);
    bool do_continue();

};



#endif
