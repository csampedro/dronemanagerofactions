//////////////////////////////////////////////////////
//  takingoff.h
//
//  Created on: Jul 24, 2015
//      Author: carlos sampedro
//
//  Last modification on: Aug 5, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef TAKINGOFF_H
#define TAKINGOFF_H


#include <iostream>
#include <string>
#include <vector>


#include "droneManager.h"


////////////////////////////////////////////
// Class Takingoff
//
//   Description
//   This class implements the manager state: 
//   takingoff
//
////////////////////////////////////////////
class Takingoff: public ManagerState
{
private:
    DroneManager* drone_manager;
    bool start_up_services_sent;
public:
    Takingoff(DroneManager *manager_mach);
    ~Takingoff(void);
    bool takeoff();
    //bool hover();
    bool hover_from_altitud();
    bool land();
    bool emergency();
    bool do_continue();
};



#endif
