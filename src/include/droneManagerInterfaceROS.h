//////////////////////////////////////////////////////
//  droneManagerInterfaceROS.h
//
//  Created on: Jul 30, 2015
//      Author: carlos sampedro
//
//  Last modification on: Jul 30, 2014
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef DRONE_MANAGER_INTERFACE_ROS_H
#define DRONE_MANAGER_INTERFACE_ROS_H


#include <iostream>
#include <string>
#include <vector>


// ROS
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "communication_definition.h"

// Flying modes
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneCommand.h"



/////////////////////////////////////////
// Class DroneManagerInterfaceROS
//
//   Description
//   This class implements the manager
//   Interface for ROS
//
/////////////////////////////////////////
class DroneManagerInterfaceROS
{
private:
    ros::NodeHandle   n;
    droneMsgsROS::droneStatus last_drone_status_msg;

public:
    DroneManagerInterfaceROS(void);
    ~DroneManagerInterfaceROS(void);

    void open(ros::NodeHandle & nIn);
    void droneStatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg);
    void missionToManagerCallback(const std_msgs::String::ConstPtr& msg);
    ros::Publisher DroneCommandPubl;
    ros::Subscriber drone_status_subscriber;
    ros::Subscriber drone_action_suscriber;

    void drone_takeOff();
    void drone_land();
    void drone_hover();
    void drone_move();


};



#endif
