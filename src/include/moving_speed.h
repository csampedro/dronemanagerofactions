//////////////////////////////////////////////////////
//  moving_speed.h
//
//  Created on: Aug 11, 2015
//      Author: carlos sampedro
//
//  Last modification on: Aug 11, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MOVING_SPEED_H
#define MOVING_SPEED_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"
#include "moving.h"


////////////////////////////////////////////
// Class MovingSpeed
//
//   Description
//   This class implements the manager state: 
//   MovingSpeed
//
////////////////////////////////////////////
class MovingSpeed : public Moving
{

public:
    MovingSpeed(DroneManager *manager_mach);
    ~MovingSpeed(void);
    bool move_speed();
    bool do_continue();

};



#endif
