//////////////////////////////////////////////////////
//  manager_inc.h
//
//  Created on: Jul 28, 2015
//      Author: carlos sampedro
//
//  Last modification on: Jul 28, 2014
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MANAGER_INC_H
#define MANAGER_INC_H

//#define DEBUG_MODE

#include "landed.h"
#include "landing.h"
#include "moving.h"
#include "hovering.h"
#include "takingoff.h"
#include "looping.h"
#include "moving_manual_altitud.h"
#include "moving_position.h"
#include "moving_speed.h"
#include "moving_trajectory.h"
#include "moving_visual_servoing.h"
#include "hovering_visual_servoing.h"
#include "emergency.h"
#include "sleeping.h"
#include "landing_autonomous.h"
#include "moving_visual_servoing_rl.h"

#endif
