//////////////////////////////////////////////////////
//  moving_manual_altitud.h
//
//  Created on: Aug 4, 2015
//      Author: carlos sampedro
//
//  Last modification on: Aug 5, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MOVING_MANUAL_ALTITUD_H
#define MOVING_MANUAL_ALTITUD_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"
#include "moving.h"


////////////////////////////////////////////
// Class MovingManualAltitud
//
//   Description
//   This class implements the manager state: 
//   MovingManualAltitud
//
////////////////////////////////////////////
class MovingManualAltitud : public Moving
{

public:
    MovingManualAltitud(DroneManager *manager_mach);
    ~MovingManualAltitud(void);
    bool move_manual_altitude();
    bool do_continue();

};



#endif
