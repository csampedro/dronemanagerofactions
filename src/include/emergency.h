//////////////////////////////////////////////////////
//  emergency.h
//
//  Created on: Aug 15, 2015
//      Author: carlos sampedro
//
//  Last modification on: Aug 15, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef EMERGENCY_H
#define EMERGENCY_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"


////////////////////////////////////////////
// Class Emergency
//
//   Description
//   This class implements the manager state: 
//   Emergency
//
////////////////////////////////////////////
class Emergency : public ManagerState
{
private:
    DroneManager* drone_manager;
public:
    Emergency(DroneManager *manager_mach);
    ~Emergency(void);
    bool land();
    bool hover();
    bool do_continue();

};



#endif
