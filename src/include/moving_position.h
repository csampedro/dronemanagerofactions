//////////////////////////////////////////////////////
//  moving_position.h
//
//  Created on: Aug 11, 2015
//      Author: carlos sampedro
//
//  Last modification on: Aug 11, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MOVING_POSITION_H
#define MOVING_POSITION_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"
#include "moving.h"


////////////////////////////////////////////
// Class MovingPosition
//
//   Description
//   This class implements the manager state: 
//   MovingPosition
//
////////////////////////////////////////////
class MovingPosition : public Moving
{

public:
    MovingPosition(DroneManager *manager_mach);
    ~MovingPosition(void);
    //bool move_position();
    bool do_continue();

};



#endif
