//////////////////////////////////////////////////////
//  moving_trajectory.h
//
//  Created on: Aug 11, 2015
//      Author: carlos sampedro
//
//  Last modification on: Aug 11, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MOVING_TRAJECTORY_H
#define MOVING_TRAJECTORY_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"
#include "moving.h"


////////////////////////////////////////////
// Class MovingTrajectory
//
//   Description
//   This class implements the manager state: 
//   MovingTrajectory
//
////////////////////////////////////////////
class MovingTrajectory : public Moving
{

public:
    MovingTrajectory(DroneManager *manager_mach);
    ~MovingTrajectory(void);
    bool do_continue();

};



#endif
