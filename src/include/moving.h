//////////////////////////////////////////////////////
//  moving.h
//
//  Created on: Jul 24, 2015
//      Author: carlos sampedro
//
//  Last modification on: Jul 24, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MOVING_H
#define MOVING_H


#include <iostream>
#include <string>
#include <vector>


#include "droneManager.h"


////////////////////////////////////////////
// Class Moving
//
//   Description
//   This class implements the manager state: 
//   moving
//
////////////////////////////////////////////
class Moving: public ManagerState
{
protected:
    DroneManager* drone_manager;
public:
    Moving(DroneManager *manager_mach);
    //Moving();
    ~Moving(void);
    virtual bool land();
    virtual bool land_autonomous();
    virtual bool hover();
    virtual bool sleep();
    virtual bool loop();
    virtual bool move_manual_altitude();
    virtual bool move_position();
    virtual bool move_speed();
    virtual bool move_trajectory();
    virtual bool move_visual_servoing();
    virtual bool move_visual_servoing_rl();
    virtual bool emergency();
    virtual bool do_continue()=0;

};



#endif
