//////////////////////////////////////////////////////
//  moving_visual_servoing.h
//
//  Created on: Sept 7, 2015
//      Author: carlos sampedro
//
//  Last modification on: Sept 7, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MOVING_VISUAL_SERVOING_H
#define MOVING_VISUAL_SERVOING_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"
#include "moving.h"


////////////////////////////////////////////
// Class MovingVisualServoing
//
//   Description
//   This class implements the manager state: 
//   MovingVisualServoing
//
////////////////////////////////////////////
class MovingVisualServoing : public Moving
{

public:
    MovingVisualServoing(DroneManager *manager_mach);
    ~MovingVisualServoing(void);
    bool hover_visual_servoing();
    bool do_continue();

};



#endif
