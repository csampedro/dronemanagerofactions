//////////////////////////////////////////////////////
//  landed.h
//
//  Created on: Jul 24, 2015
//      Author: carlos sampedro
//
//  Last modification on: Jul 24, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef LANDED_H
#define LANDED_H


#include <iostream>
#include <string>
#include <vector>


#include "droneManager.h"


////////////////////////////////////////////
// Class Landed
//
//   Description
//   This class implements the manager state: 
//   landed
//
////////////////////////////////////////////
class Landed: public ManagerState
{
private:
    DroneManager* drone_manager;
public:
    Landed(DroneManager *manager_mach);
    ~Landed(void);
    bool land();
    bool takeoff();
    bool do_continue();
};



#endif
