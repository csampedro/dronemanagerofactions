//////////////////////////////////////////////////////
//  hovering.h
//
//  Created on: Jul 24, 2015
//      Author: carlos sampedro
//
//  Last modification on: Jul 24, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef HOVERING_H
#define HOVERING_H


#include <iostream>
#include <string>
#include <vector>
#include "droneManager.h"



////////////////////////////////////////////
// Class Hovering
//
//   Description
//   This class implements the manager state: 
//   hovering
//
////////////////////////////////////////////
class Hovering: public ManagerState
{
protected:
    DroneManager* drone_manager;
public:
    Hovering(DroneManager* manager_mach);
    ~Hovering(void);
    bool land();
    bool loop();
    virtual bool hover();
    bool move_manual_altitude();
    bool move_position();
    bool move_speed();
    bool move_trajectory();
    bool move_visual_servoing();
    bool emergency();
    virtual bool do_continue();

};



#endif
