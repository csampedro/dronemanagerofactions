//////////////////////////////////////////////////////
//  looping.h
//
//  Created on: Sept 10, 2015
//      Author: carlos sampedro
//
//  Last modification on: Sept 10, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef LOOPING_H
#define LOOPING_H


#include <iostream>
#include <string>
#include <vector>


#include "droneManager.h"


////////////////////////////////////////////
// Class Looping
//
//   Description
//   This class implements the manager state: 
//   Looping
//
////////////////////////////////////////////
class Looping: public ManagerState
{
private:
    DroneManager* drone_manager;
public:
    Looping(DroneManager *manager_mach);
    ~Looping(void);
    bool hover();
    bool hover_from_altitud();
    bool do_continue();
};



#endif
