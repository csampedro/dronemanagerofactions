//////////////////////////////////////////////////////
//  moving_visual_servoing_rl.h
//
//  Created on: Aug 23, 2017
//      Author: carlos sampedro
//
//  Last modification on: Aug 23, 2017
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef MOVING_VISUAL_SERVOING_RL_H
#define MOVING_VISUAL_SERVOING_RL_H


#include <iostream>
#include <string>
#include <vector>

#include "droneManager.h"
#include "moving.h"


////////////////////////////////////////////
// Class MovingVisualServoingRl
//
//   Description
//   This class implements the manager state: 
//   MovingVisualServoing for Reinforcement
//   Learning
//
////////////////////////////////////////////
class MovingVisualServoingRl : public Moving
{

public:
    MovingVisualServoingRl(DroneManager *manager_mach);
    ~MovingVisualServoingRl(void);
    bool hover_visual_servoing();
    bool do_continue();

};



#endif
