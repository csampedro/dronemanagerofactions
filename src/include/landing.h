//////////////////////////////////////////////////////
//  landing.h
//
//  Created on: Jul 24, 2015
//      Author: carlos sampedro
//
//  Last modification on: Jul 24, 2015
//      Author: carlos sampedro
//
//////////////////////////////////////////////////////



#ifndef LANDING_H
#define LANDING_H


#include <iostream>
#include <string>
#include <vector>


#include "droneManager.h"


////////////////////////////////////////////
// Class Landing
//
//   Description
//   This class implements the manager state: 
//   landing
//
////////////////////////////////////////////
class Landing: public ManagerState
{
private:
    DroneManager* drone_manager;
public:
    Landing(DroneManager *manager_mach);
    ~Landing(void);
    bool land();
    bool takeoff();
    bool land_from_altitud();
    bool do_continue();
};



#endif
